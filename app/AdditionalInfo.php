<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdditionalInfo extends Model
{
    protected $table = 'additionalinfo';
    protected $fillable = ['art','movies','music'
	];
	protected $hidden = [
        'id', 'created_at', 'updated_at', 'infobasic_id'
    ];
    public function infobasic()
    {
        return $this->hasOne('App\InfoBasic');
    }
}
