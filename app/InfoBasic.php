<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoBasic extends Model
{
    protected $table = 'infobasic';
    protected $fillable = ['firstname','lastname','phonenumber', 'email', 'address'
	];

	public function additional()
    {
        return $this->hasOne('App\AdditionalInfo', 'infobasic_id');
    }

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
