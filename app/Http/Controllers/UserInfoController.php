<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InfoBasic;
use App\AdditionalInfo;
use Illuminate\Http\Response;

class UserInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$info = InfoBasic::all();
        $info = InfoBasic::with('additional')->get();
        return response()->json($info); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$info = $this->request->all();

        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
            'address' => 'required',
            'phonenumber' => 'required',
            'email' => 'required',
        ]);

        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');
        $address = $request->input('address');
        $phonenumber = $request->input('phonenumber');
        $email = $request->input('email');

        $infobasic = InfoBasic::create([
            'firstname' => $firstname,
            'lastname' => $lastname,
            'address' => $address,
            'phonenumber' => $phonenumber,
            'email' => $email,
        ]);

        $art = $request->input('art');
        $movies = $request->input('movies');
        $music = $request->input('music');
        $infobasic_id = InfoBasic::orderBy('id', 'desc')->first();
        
        $additionalinfo = $infobasic_id->additional()->create([
            'art' => $art,
            'movies' => $movies,
            'music' => $music,
        ]);

        return response()->json(['status' => Response::HTTP_CREATED]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $info = InfoBasic::with('additional')->find($id);
        return response()->json($info); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');
        $address = $request->input('address');
        $phonenumber = $request->input('phonenumber');
        $email = $request->input('email');

        InfoBasic::find($id)->update([
            'firstname' => $firstname,
            'lastname' => $lastname,
            'address' => $address,
            'phonenumber' => $phonenumber,
            'email' => $email,
        ]);

        return response()->json(['status' => Response::HTTP_OK]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        InfoBasic::find($id)->delete();
        return response()->json(['status' => Response::HTTP_OK]);
    }
}
