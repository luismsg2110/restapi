<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdditionalinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additionalinfo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('art');
            $table->string('movies');
            $table->string('music');
            $table->unsignedInteger('infobasic_id');
            $table->foreign('infobasic_id')->references('id')->on('infobasic')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('additionalinfo');
    }
}
