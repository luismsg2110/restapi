<?php

use Illuminate\Database\Seeder;

class AdditionalInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $infoBasicID = DB::table('infobasic')->pluck('id');
        DB::table('additionalinfo')->insert([
            'art' => 'Picasso',
            'movies' => 'Piratas del Caribe',
            'music' =>'Linkin Park',
            'infobasic_id' => '1',
        ]);
    }
}
