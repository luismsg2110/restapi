<?php

use Illuminate\Database\Seeder;

class InfoBasicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('infobasic')->insert([
            'firstname' => 'Luis',
            'lastname' => 'Sanchez',
            'phonenumber' =>'1111',
            'email' => 'luismsg2110@gmail.com',
            'address' => 'Engativa',
        ]);
    }
}
