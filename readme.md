## Prueba

Fué desarrollada con el framework Laravel de PHP, a continuación se describen los pasos para correr la aplicación:

- Instalar composer y laravel
- Instalar y configurar MySql, asi como crear una base de datos (utf8mb).
- Configurar el archivo .env con los datos de la base de datos (nombre, usario, password).
- Ejecutar las migraciones y los seeds con: `php artisan migrate:refresh --seed`
- Correr la aplicación con: `php artisan serve`

## Creación de la aplicación:

### 1) Migraciones
Se crearon dos migraciones para crear la tabla **infobasic** (con la información básica del usario) y la tabla **additionalinfo** (con la información adicional del usario)

#### Ejemplo de infobasic

```php
	Schema::create('infobasic', function (Blueprint $table) {
       $table->increments('id');
       $table->string('firstname');
       $table->string('lastname');
       $table->integer('phonenumber');
       $table->string('email')->unique();
       $table->string('address');
       $table->timestamps();
   	});		
```

### 2) Seeder
Se creó para insertar data de prueba (un registro en ambas tablas):

#### Ejemplo InfoBasicSeeder

```php
		DB::table('infobasic')->insert([
            'firstname' => 'Luis',
            'lastname' => 'Sanchez',
            'phonenumber' =>'1111',
            'email' => 'luismsg2110@gmail.com',
            'address' => 'Engativa',
        ]);	
```

### 3) Modelo	
En este modelo se especifica los metodos **additional()** en InfoBasic e **infobasic()** en AdditionalInfo   para realizar operaciones entre tablas relacionadas.
En este caso tenemos que la tabla infobasic (cuya clave primaria es 'id') se relaciona uno a uno con la tabla additionalinfo (cuya clave foranea es 'infobasic_id').

### 4) Ruta	
En este caso se creo la ruta 'api/rest', accediendo a los recursos a través del controlador *UserInfoController*.
Lista de Rutas

- **Solicitar la información de todos los usuarios registrados**
- Metodo: GET
- URI: api/user
- Resultado: se obtiene toda la información (básica + adicional) de todos los usuarion en formato JSON

- **Crear nuevos usuarios**
- Metodo: POST
- URI: api/user
- Request: Información de usario a insertar
- Resultado: Status Code de la petición

- **Eliminar un usuario**
- Metodo: DELETE
- URI: api/user/$id
- Resultado:  Status Code de la petición

- **Actualizar un usario**
- Metodo: PUT
- URI: api/user/$id
- Request: Información de usario a actualizar
- Resultado: Status Code de la petición

- **Mostrar información de un usuario en específico**
- Metodo: GET
- URI: api/user/$id
- Resultado: se obtiene toda la información (básica + adicional) del usuario en formato JSON

### 5) Controllador
Se creó el controllador *UserInfoController* para crear los métodos que nos permiten ver, crear, editar y eliminar los recursos.

## Pruebas de la aplicación
A continuación se muestra capturas de las pruebas realizadas en Postman.

a) Información de todos los usuarios

![Obtener todos los usuarios](https://luismsg2110.000webhostapp.com/images/testapi/alluser.png)

b) Crear un nuevo usuario

![Crear un nuevo usuario](https://luismsg2110.000webhostapp.com/images/testapi/createuser.png)

c) Eliminar a un usuario

![Eliminar a un usuario](https://luismsg2110.000webhostapp.com/images/testapi/deleteuser.png)

d) Actualizar a un usuario

![Actualizar a un usuario](https://luismsg2110.000webhostapp.com/images/testapi/updateuser.png)

Cabe destacar, en este caso al ser un Update en el Postman se debe colocar en el Body la opción x-www-form-urlencoded.

e) Obtener información de un usario

![Obtener información de un usario](https://luismsg2110.000webhostapp.com/images/testapi/oneuser.png)

